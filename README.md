# impatient-sqlite-jdbc

This project implements the interfaces in `impatient-sqlite` by wrapping a JDBC database.
This project does not know how to connect to a database - see `impatient-sqlite-main` for that.