package imp.sqlite.jdbc

import imp.sqlite.Database
import org.slf4j.LoggerFactory
import java.sql.Connection

/**A wrapper for a JDBC database Connection. */
class JdbcDatabase(private val conn: Connection) : Database {
	companion object {
		private val log = LoggerFactory.getLogger(JdbcDatabase::class.java)

		init { Class.forName("org.sqlite.JDBC") }
	}

	/**0 means we are not in a transaction. */
	private var transactionCounter = 0
	override val inTransaction = transactionCounter != 0

	override fun beginTransaction() {
		log.trace("Beginning transaction with counter at {}", transactionCounter)
		if (transactionCounter++ == 0) {
			conn.autoCommit = false
		}
	}

	override fun commit() {
		log.trace("Committing transaction with counter at {}", transactionCounter)
		check(transactionCounter != 0) { "No transaction to commit" }
		if (--transactionCounter == 0) {
			conn.commit()
			conn.autoCommit = true
		}
	}

	override fun rollback() {
		log.trace("Rolling back transaction with counter at {}", transactionCounter)
		check(transactionCounter != 0) { "There is no transaction to roll back." }
		conn.rollback()
		conn.autoCommit = true
		transactionCounter = 0
	}

	override fun prepareInsert(sql: String) = prepare(sql)
	override fun prepareSelect(sql: String) = prepare(sql)
	override fun prepareUpdate(sql: String) = prepare(sql)

	private fun prepare(sql: String): JdbcPreparedStatement {
		log.trace("Preparing: {}", sql)
		return JdbcPreparedStatement(conn.prepareStatement(sql))
	}

	override fun close() = conn.close()

	override fun enforceForeignKeys(enforce: Boolean) {
		//this.exec("PRAGMA foreign_keys = OFF")
		conn.createStatement().use { stmt ->
			stmt.execute("PRAGMA foreign_keys = " + if(enforce) "ON" else "OFF")
		}
	}
}