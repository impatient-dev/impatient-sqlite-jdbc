package imp.sqlite.jdbc

import JdbcCursor
import imp.sqlite.PreparedInsert
import imp.sqlite.PreparedSelect
import imp.sqlite.PreparedUpdate
import java.sql.PreparedStatement
import java.sql.Types

/**Since JDBC indexes are 1-based but ours are 0-based, passed-in indexes have to be incremented.*/
class JdbcPreparedStatement (
	private val stmt: PreparedStatement
) : PreparedInsert, PreparedSelect, PreparedUpdate {
	override fun setNull(i: Int) = stmt.setNull(i+1, Types.NULL)
	override fun setBlob(i: Int, value: ByteArray) = stmt.setBytes(i+1, value)
	override fun setBoolean(i: Int, value: Boolean) = stmt.setBoolean(i+1, value)
	override fun setDouble(i: Int, value: Double) = stmt.setDouble(i+1, value)
	override fun setFloat(i: Int, value: Float) = stmt.setFloat(i+1, value)
	override fun setInt(i: Int, value: Int) = stmt.setInt(i+1, value)
	override fun setLong(i: Int, value: Long) = stmt.setLong(i+1, value)
	override fun setShort(i: Int, value: Short) = stmt.setShort(i+1, value)

	override fun setString(i: Int, value: String) = stmt.setString(i+1, value)
	override fun setParam(i: Int, value: Any?) = if(value == null) setNull(i) else stmt.setObject(i+1, value)

	override fun update() {
		stmt.execute()
	}

	override fun insert(): Any {
		stmt.execute()
		stmt.generatedKeys.use { keys ->
			if(keys.next())
				return keys.getObject(1) ?: throw Exception("Null primary key generated")
			else
				throw RuntimeException("No keys were generated.")
		}
	}

	override fun select() = JdbcCursor(stmt.executeQuery())

	override fun close() = stmt.close()
}