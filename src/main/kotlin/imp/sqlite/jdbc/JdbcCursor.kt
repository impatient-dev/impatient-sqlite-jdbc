import imp.sqlite.Cursor
import java.sql.ResultSet

/**A wrapper for a JDBC ResultSet. */
class JdbcCursor (
	private val rs: ResultSet,
) : Cursor {

	override fun moveToNext() = rs.next()

	private inline fun <T> nullable(block: () -> T): T? {
		val out = block()
		return if(rs.wasNull()) null else out
	}

	override fun getBlob(column: Int) = nullable { rs.getBytes(column + 1) }
	override fun getBoolean(column: Int) = nullable { rs.getBoolean(column + 1) }
	override fun getDouble(column: Int) = nullable { rs.getDouble(column + 1) }
	override fun getFloat(column: Int) = nullable { rs.getFloat(column + 1) }
	override fun getInt(column: Int) = nullable { rs.getInt(column + 1) }
	override fun getLong(column: Int) = nullable { rs.getLong(column + 1) }
	override fun getShort(column: Int) = nullable { rs.getShort(column + 1) }

	override fun getString(column: Int): String? = rs.getString(column + 1)

	override fun columnIndex(name: String) = rs.findColumn(name)

	override fun getBlob(column: String) = nullable { rs.getBytes(column) }
	override fun getBoolean(column: String) = nullable { rs.getBoolean(column) }
	override fun getDouble(column: String) = nullable { rs.getDouble(column) }
	override fun getFloat(column: String) = nullable { rs.getFloat(column) }
	override fun getInt(column: String) = nullable { rs.getInt(column) }
	override fun getLong(column: String) = nullable { rs.getLong(column) }
	override fun getShort(column: String) = nullable { rs.getShort(column) }

	override fun getString(column: String): String? = rs.getString(column)

	override fun close() = rs.close()
}
