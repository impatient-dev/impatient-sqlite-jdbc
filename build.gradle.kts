plugins {
	id("org.jetbrains.kotlin.jvm")
}

group = "imp-dev"
version = "0.0.0"

repositories {
	mavenCentral()
}

dependencies {
	implementation(project(":impatient-sqlite"))
	implementation(project(":impatient-utils"))
}